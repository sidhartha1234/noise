#!/bin/sh

readonly CDDB_URL=http://freedb.freedb.org/~cddb/cddb.cgi
readonly CDDB_PROTO=6
readonly CDDB_USER=user
readonly CDDB_HOST=host

readonly DISCID_FILE=discid
readonly CDDBQUERY_FILE=cddb-query
readonly CDDBREAD_FILE=cddb-read
readonly TOC_FILE=toc
readonly DISC_TAGS_FILE=tags-disc
readonly TAGS_FILE=tags

readonly SAMPLE_OFFSET=6

readonly DISCID=$(cd-discid | cut -d' ' -f1)
readonly OUTDIR=$PWD/$DISCID

read_discid()
{
  cd-discid > "$DISCID_FILE"
}

read_toc()
{
  # Don't use any of the plextor drivers as they mess up the ISRC code of the first and second track!
  cdrdao read-toc --device /dev/cdrom --driver generic-mmc "$TOC_FILE"
}

read_cddb()
{
  cddb-tool query "$CDDB_URL" "$CDDB_PROTO" "$CDDB_USER" "$CDDB_HOST" $(cat "$DISCID_FILE") > "$CDDBQUERY_FILE"

  case $(head -n1 "$CDDBQUERY_FILE" | cut -d' ' -f1) in
    200)
      cddb-tool read "$CDDB_URL" "$CDDB_PROTO" "$CDDB_USER" "$CDDB_HOST" $(cut -d' ' -f2,3 "$CDDBQUERY_FILE") > "$CDDBREAD_FILE-1"
      ;;
    210)
      # Loop over lines containing matches. Strip first and last line which
      # contain status and end marker. Use grep to number the lines.
      local line
      cat "$CDDBQUERY_FILE" \
        | tail -n +2 | head -n -1 | grep -n '^' \
        | while IFS= read -r line || [ -n "$line" ] # http://mywiki.wooledge.org/DontReadLinesWithFor
      do
        # Extract the match number and payload.
        local readonly N=$(echo "$line" | cut -d':' -f1)
        local readonly MATCH=$(echo "$line" | cut -d':' -f2-) # Include all fields in case they contain colons.
        cddb-tool read "$CDDB_URL" "$CDDB_PROTO" "$CDDB_USER" "$CDDB_HOST" $(echo "$MATCH" | cut -d' ' -f1,2) > "$CDDBREAD_FILE-$N"
      done
      ;;
    *)
      echo unexpected status in $CDDBQUERY_FILE
      exit 1
      ;;
  esac

  # TODO prompt to select specific cddbread file in case of multiple matches
}

# Strip whitespace before the first argument and after the last argument.
# Whitespace between arguments is condensed into a single space.
trim()
{
  echo "$@" | sed 's/^\s\+\|\s\+$//g'
}

tags_clear()
{
  echo -n > "$TAGS_FILE"
}

tags_emit_pair()
{
  echo "$1=$2" >> "$TAGS_FILE"
}

tags_emit_note()
{
  echo '--' "$1" >> "$TAGS_FILE"
}

tags_emit_line()
{
  echo >> "$TAGS_FILE"
}

# Calculate the MusicBrainz disc ID according to https://musicbrainz.org/doc/Disc_ID_Calculation
get_musicbrainz_discid()
{
  local readonly DISCID_TRACK_TOTAL=$(cut -d' ' -f2 "$DISCID_FILE")
  local readonly AUDIO_TRACK_TOTAL=$(cat "$TOC_FILE" | grep '^TRACK AUDIO$' | wc -l)

  local LEAD_OUT_OFFSET

  # TODO Use cdrecord's toc in both cases. Need to figure out a way to identify the range of audio tracks and position of the lead-out track.
  if [ "$DISCID_TRACK_TOTAL" == "$AUDIO_TRACK_TOTAL" ]
  then
    # Without data tracks, the discid output provides no offset for the lead-out track. We have to determine it via cdrecord.
    LEAD_OUT_OFFSET=$(cdrecord dev=/dev/cdrom -toc 2>/dev/null | grep -o '^track:lout\s\+lba:\s\+[0-9]\+' | grep -o '[0-9]\+')
    LEAD_OUT_OFFSET=$(($LEAD_OUT_OFFSET + 150))
  else
    # The discid output also provides offsets for data tracks which can be used to determine the offset of the lead-out track.
    LEAD_OUT_OFFSET=$(cut -d' ' -f$((3 + $AUDIO_TRACK_TOTAL)) "$DISCID_FILE")
    LEAD_OUT_OFFSET=$(($LEAD_OUT_OFFSET - 11400))
  fi

  TRACK_OFFSETS=$(cut -d' ' -f3-$((2 + $AUDIO_TRACK_TOTAL)) "$DISCID_FILE")
  REST_OFFSETS=$(yes 0 | head -n $((99 - $AUDIO_TRACK_TOTAL)))

  echo -n $(printf '%02X' 1 "$AUDIO_TRACK_TOTAL")$(printf '%08X' $LEAD_OUT_OFFSET $TRACK_OFFSETS $REST_OFFSETS) \
    | sha1sum | cut -d' ' -f1 | xxd -r -p | base64 | tr '+/=' '._-'
}

write_tags()
{
  echo Write tags to "$TAGS_FILE"

  local readonly DTITLE=$(grep -P "^DTITLE=" "$CDDBREAD_FILE-1" | sed 's/^DTITLE=//g')

  local readonly ARTIST=$(trim $(echo "$DTITLE" | cut -d'/' -f1))
  local readonly ALBUM=$(trim $(echo "$DTITLE" | cut -d'/' -f2-))
  local readonly YEAR=$(trim $(grep "^DYEAR=" "$CDDBREAD_FILE-1" | sed 's/^DYEAR=//g'))

  # Get the total track number from the toc file instead the discid output.
  # The latter will include data tracks in its count.
  local readonly TRACKTOTAL=$(grep '^TRACK AUDIO$' "$TOC_FILE" | wc -l)

  local readonly MUSICBRAINZ=$(get_musicbrainz_discid)

  tags_clear
  tags_emit_note 'Tags applying to all tracks on the disc.'
  tags_emit_line
  tags_emit_pair ARTIST "$ARTIST"
  tags_emit_pair ALBUM "$ALBUM"
  # CDDB's DYEAR document the year of the release.
  tags_emit_pair RELEASEDATE "$YEAR"
  tags_emit_pair DISCNUMBER 1 # TODO provide as optional command line arguments
  tags_emit_pair DISCTOTAL 1 # TODO provide as optional command line arguments
  tags_emit_pair FREEDB "$DISCID"
  tags_emit_pair MUSICBRAINZ "$MUSICBRAINZ"
  tags_emit_pair TRACKTOTAL "$TRACKTOTAL"
  tags_emit_line
  tags_emit_note 'Tags applying to individual tracks.'
  tags_emit_note 'The first tag must be TRACKNUMBER which is used to match the tags with corresponding tracks.'
  tags_emit_line
  tags_emit_note 'Potential hidden track (http://wiki.hydrogenaud.io/index.php?title=HTOA).'
  tags_emit_note 'Will be ignored if no such track is ripped.'
  tags_emit_line
  tags_emit_pair TRACKNUMBER 0
  tags_emit_pair TITLE
  # Vorbis Comment proposes DATE for the date the track was recorded (https://xiph.org/vorbis/doc/v-comment.html#fieldnames).
  # Provide the release year as an initial "guess".
  tags_emit_pair DATE "$YEAR"

  local n
  for n in $(seq 1 $TRACKTOTAL)
  do
    # CDDB track titles are zero-based.
    local readonly TITLE=$(trim $(grep -P "^TTITLE$(($n-1))=" "$CDDBREAD_FILE-1" | sed 's/^TTITLE[0-9]\+=//g'))
    local readonly ISRC=$(grep '^ISRC' "$TOC_FILE" | awk NR==$n | sed 's/^ISRC "\|"$//g')

    tags_emit_line
    tags_emit_pair TRACKNUMBER "$n"
    tags_emit_pair TITLE "$TITLE"
    # Vorbis Comment proposes DATE for the date the track was recorded (https://xiph.org/vorbis/doc/v-comment.html#fieldnames).
    # Provide the release year as an initial "guess".
    tags_emit_pair DATE "$YEAR"
    tags_emit_pair ISRC "$ISRC"
  done
}

read_tracks()
{
  cd-paranoia --batch --output-wav --sample-offset "$SAMPLE_OFFSET"

  # cd-paranoia automatically numbers tracks when ripped in batch mode. These
  # numbers are single digits if the total number of tracks is less than 10.
  # Normalize track numbers to two digits to provide a uniform naming scheme.
  # Also get rid of the ".cdda" part.
  local f
  for f in $(ls *.wav)
  do
    mv "$f" $(echo "$f" | sed 's/\.cdda//g' | sed 's/\([^0-9]\)\([0-9]\)\([^0-9]\)/\10\2\3/g')
  done
}

encode()
{
  flac --verify --replay-gain -8 *.wav
}

apply_tags()
{
  # Give users a chance to edit tags and wait until the file is closed.
  echo -n "Waiting for \"$TAGS_FILE\" "
  while true
  do
    X=$(lsof | grep "$TAGS_FILE")
    if test -z "$X"
    then
      break
    fi
    echo -n .
    sleep 1
  done
  echo

  local n=-1 # -1 marks tags applying to all tracks
  local line
  cat "$TAGS_FILE" \
    | while IFS= read -r line || [ -n "$line" ] # http://mywiki.wooledge.org/DontReadLinesWithFor
  do
    # Ignore empty lines and comments.
    local readonly LINE=$(echo "$line" | grep -v '^\s*--' | sed 's/\s\+$//g')
    if test -z "$LINE"
    then
      continue
    fi
    local readonly _N=$(echo "$line" | grep '^TRACKNUMBER=' | grep -o '[0-9]\+')
    if test ! -z "$_N"
    then
      n=$(printf '%02d' "$_N")
    fi
    if test "-1" -eq "$n"
    then
      # Sets tag applying to all tracks.
      metaflac --set-tag="$LINE" *.flac
    else
      local readonly FILE="track$n.flac"
      # Test that a track with the read number actually exists. Important to
      # gracefully handle potential hidden tracks.
      if test -f "$FILE"
      then
        metaflac --set-tag="$LINE" "$FILE"
      fi
    fi
  done
}

cleanup()
{
  rm *.wav
}

# Prevent accidental override ov existing rips in case of disc ID collisions.
if [ -d "$OUTDIR" ]
then
  echo "Output directory "\"$OUTDIR\"" exists. Aborting."
  exit 1
fi

mkdir -p "$OUTDIR"

# cd into output directory because the output directoy of cd-paranoia cannot be set in batch mode.
cd "$OUTDIR"

read_discid
read_toc
read_cddb
write_tags
read_tracks
encode
apply_tags
cleanup

cd -
